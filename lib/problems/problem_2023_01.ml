let year = 2023

let day = 1

module Part_1 = struct
  let value_parts_of_line line =
    String.fold_left
      (fun acc char ->
        let current_value =
          if
            List.exists (( = ) char)
              ['0'; '1'; '2'; '3'; '4'; '5'; '6'; '7'; '8'; '9']
          then Some char
          else None
        in
        match (acc, current_value) with
        | (None, None), Some i ->
            (Some i, Some i)
        | (Some x, _), Some i ->
            (Some x, Some i)
        | acc, _not_number ->
            acc )
      (None, None) line

  let run (input : string) : (string, string) result =
    let lines = String.split_on_char '\n' input in
    let values =
      List.map
        (fun line ->
          match value_parts_of_line line with
          | Some x, Some y ->
              int_of_string @@ String.cat (Char.escaped x) (Char.escaped y)
          | _ ->
              0 )
        lines
    in
    let sum = List.fold_left ( + ) 0 values in
    Ok (string_of_int sum)
end

module Part_2 = struct
  let digits =
    [ ("0", 0)
    ; ("1", 1)
    ; ("2", 2)
    ; ("3", 3)
    ; ("4", 4)
    ; ("5", 5)
    ; ("6", 6)
    ; ("7", 7)
    ; ("8", 8)
    ; ("9", 9)
    ; ("one", 1)
    ; ("two", 2)
    ; ("three", 3)
    ; ("four", 4)
    ; ("five", 5)
    ; ("six", 6)
    ; ("seven", 7)
    ; ("eight", 8)
    ; ("nine", 9) ]

  let starting_digit s =
    List.find_map
      (fun (digit_s, digit_i) ->
        if String.starts_with ~prefix:digit_s s then Some digit_i else None )
      digits

  let value_parts_of_line line =
    let rec traverse_for_parts start end' = function
      | "" ->
          (start, end')
      | s ->
          let digit = starting_digit s in
          let start, end' =
            match (start, end', digit) with
            | None, None, Some i ->
                (Some i, Some i)
            | Some x, _, Some i ->
                (Some x, Some i)
            | s, e, _not_number ->
                (s, e)
          in
          traverse_for_parts start end' @@ String.sub s 1 (String.length s - 1)
    in
    traverse_for_parts None None line

  let run (input : string) : (string, string) result =
    let lines = String.split_on_char '\n' input in
    let values =
      List.map
        (fun line ->
          match value_parts_of_line line with
          | Some x, Some y ->
              int_of_string @@ String.cat (string_of_int x) (string_of_int y)
          | _ ->
              0 )
        lines
    in
    let sum = List.fold_left ( + ) 0 values in
    Ok (string_of_int sum)
end
