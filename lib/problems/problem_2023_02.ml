let year = 2023

let day = 2

module Part_1 = struct
  type extract = {red: int; green: int; blue: int}

  type game = {index: int; extracts: extract list}

  let extract_color_of_strings color sl =
    List.find_map
      (fun s ->
        match String.split_on_char ' ' s with
        | [amount; color'] ->
            if color' = color then Some (int_of_string amount) else None
        | _ ->
            None )
      sl
    |> Option.value ~default:0

  let index_of_string s =
    match String.split_on_char ' ' s with
    | [_game; index] ->
        int_of_string index
    | _ ->
        0

  (* input: 3 blue, 4 red *)
  let extract_of_string s =
    let extract_parts = String.split_on_char ',' s |> List.map String.trim in
    { red= extract_color_of_strings "red" extract_parts
    ; green= extract_color_of_strings "green" extract_parts
    ; blue= extract_color_of_strings "blue" extract_parts }

  let game_of_line line =
    match String.split_on_char ':' line with
    | [id; extracts] ->
        let extracts =
          String.split_on_char ';' (String.trim extracts)
          |> List.map String.trim
        in
        { index= index_of_string id
        ; extracts= List.map extract_of_string extracts }
        (* ; extracts= List.map (fun i -> let n = extract_of_string i in Printf.printf "red %d green %d blue %d\n" n.red n.green n.blue; n) extracts } *)
    | _ ->
        {index= 0; extracts= []}

  let run (input : string) : (string, string) result =
    let lines = String.split_on_char '\n' input in
    List.map game_of_line lines
    |> List.filter (fun game ->
           List.for_all
             (fun extract ->
               extract.red <= 12 && extract.green <= 13 && extract.blue <= 14 )
             game.extracts )
    |> List.fold_left (fun acc game -> acc + game.index) 0
    |> string_of_int
    (* |> List.fold_left (fun acc game -> acc ^ (string_of_int game.index) ^ "\n") "" *)
    |> Result.ok
end

module Part_2 = struct
  type extract = {red: int; green: int; blue: int}

  type game = {index: int; extracts: extract list}

  let extract_color_of_strings color sl =
    List.find_map
      (fun s ->
        match String.split_on_char ' ' s with
        | [amount; color'] ->
            if color' = color then Some (int_of_string amount) else None
        | _ ->
            None )
      sl
    |> Option.value ~default:0

  let index_of_string s =
    match String.split_on_char ' ' s with
    | [_game; index] ->
        int_of_string index
    | _ ->
        0

  (* input: 3 blue, 4 red *)
  let extract_of_string s =
    let extract_parts = String.split_on_char ',' s |> List.map String.trim in
    { red= extract_color_of_strings "red" extract_parts
    ; green= extract_color_of_strings "green" extract_parts
    ; blue= extract_color_of_strings "blue" extract_parts }

  let game_of_line line =
    match String.split_on_char ':' line with
    | [id; extracts] ->
        let extracts =
          String.split_on_char ';' (String.trim extracts)
          |> List.map String.trim
        in
        { index= index_of_string id
        ; extracts= List.map extract_of_string extracts }
    | _ ->
        {index= 0; extracts= []}

  let run (input : string) : (string, string) result =
    let lines = String.split_on_char '\n' input in
    List.map game_of_line lines
    |> List.map (fun game ->
           let {red= max_red; green= max_green; blue= max_blue} =
             List.fold_left
               (fun acc extract ->
                 { red= max acc.red extract.red
                 ; green= max acc.green extract.green
                 ; blue= max acc.blue extract.blue } )
               {red= 0; green= 0; blue= 0}
               game.extracts
           in
           max_red * max_green * max_blue )
    |> List.fold_left ( + ) 0 |> string_of_int |> Result.ok
end
